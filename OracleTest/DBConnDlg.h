#pragma once
#include <wx\wx.h>

#define RBUTCLOSE 1
#define RBUTCONN 2

class CDBConnDlg :
    public wxDialog {
    CDBConnDlg();
    wxButton *butclose = nullptr;
    wxButton *butconnect = nullptr;
    wxTextCtrl *textconnstr = nullptr;
    std::string strconn;

public:
    void OnButConnect(wxCommandEvent &event);
    void OnButClose(wxCommandEvent &event);

    CDBConnDlg(wxWindow *parent, wxWindowID id, std::string _strconn) :wxDialog(parent, id, "Verbindungsdialog"),strconn(_strconn) {
        SetSize(400, 150);
        Center();

        textconnstr = new wxTextCtrl(this, wxID_ANY, strconn, { 10,10 }, { 360,50 });

        butconnect = new wxButton(this, RBUTCONN, "Verbinden", { 10, 70 }, { 75,30 });


        auto sztc = textconnstr->GetSize();

        butclose = new wxButton(this, RBUTCLOSE, "Beenden", { 10 + sztc.GetWidth()-75, 70 }, {75,30});
    };

    std::string GetConnStr();

    ~CDBConnDlg();

    DECLARE_EVENT_TABLE()
};


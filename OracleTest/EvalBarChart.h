#pragma once

#include "EvAlChart.h"
#include <memory>
#include <algorithm>

class EvAlBarChartData {
private:
    std::string strtitle = "";
    std::string stryaxlbl = "";
    std::string strxaxlbl = "";

    std::vector<std::string> vaxisdata = {};
    std::vector<int> vdata = {};
    std::vector<std::string> vbarlabels = {};
    int valmax = 0;
public:
    auto GetYAxisLabel() {
        return stryaxlbl;
    };

    auto GetXAxisLabel() {
        return strxaxlbl;
    };

    auto GetTitle() {
        return strtitle;
    };

    auto GetValMax() {
        return valmax;
    }
    auto &GetVBarLabels() {
        return vbarlabels;
    };

    auto &GetVAxisData() {
        return vaxisdata;
    };

    auto &GetVData() {
        return vdata;
    };

    EvAlBarChartData() {};

    EvAlBarChartData(std::string _strtitle, std::string _stryaxlbl, std::string _strxaxlbl, std::vector<std::string> _vaxisdata, std::vector<int> _vdata, std::vector<std::string> _vbarlabels = {}) :strtitle(_strtitle), stryaxlbl(_stryaxlbl), strxaxlbl(_strxaxlbl), vaxisdata(_vaxisdata), vdata(_vdata),vbarlabels(_vbarlabels) {
        if(vaxisdata.size() != vdata.size() && (vbarlabels.size()==0 || vbarlabels.size()!=vaxisdata.size())) {
            throw std::exception("Axis and data size mismatching.");
        };

        valmax = *std::max_element(vdata.begin(), vdata.end());

    };
    ~EvAlBarChartData() {

    };
};

class EvAlChart;

class EvAlBarChart:public EvAlChart {
private:
    const unsigned int barsperpage = 40;
    const unsigned int scrollnumbars = 5;
    int valstart = 0;
    std::shared_ptr<EvAlBarChartData> data;
public:
    virtual bool IsChartScrollable();
    //virtual void Draw(wxAutoBufferedPaintDC  *pdc);
    virtual void Draw(wxBufferedPaintDC *pdc, wxSize szpaintarea);
    virtual void Scroll(int dir); /* 0 = up, 1 = down*/
    void SetData(std::shared_ptr<EvAlBarChartData> data);
    void SetPage(int page);
    EvAlBarChart();
    ~EvAlBarChart();
};


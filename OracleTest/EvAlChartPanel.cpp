#include "stdafx.h"
#include "EvAlChartPanel.h"

wxBEGIN_EVENT_TABLE(EvAlChartPanel, wxPanel)
    EVT_PAINT(OnPaint)
    EVT_MOUSEWHEEL(OnMousewheel)
    EVT_ENTER_WINDOW(OnMouseEnter)
    EVT_SIZE(OnSize)
wxEND_EVENT_TABLE()

void EvAlChartPanel::OnPaint(wxPaintEvent &event) {
    wxBufferedPaintDC  *dc = new wxBufferedPaintDC(this);

    chart->Draw(dc, GetSize());

    delete dc;
}

void EvAlChartPanel::OnMousewheel(wxMouseEvent &event) {
    chart->Scroll(event.GetWheelRotation());
    Refresh();

}

void EvAlChartPanel::OnMouseEnter(wxMouseEvent &event) {
    if(chart->IsChartScrollable())
        SetToolTip("(Mausrad bewegen um durch das Diagramm zu scrollen)");
    else
        SetToolTip("");

}

void EvAlChartPanel::OnSize(wxSizeEvent &event) {
    Refresh();
}


EvAlChartPanel::EvAlChartPanel() {

}


EvAlChartPanel::~EvAlChartPanel() {

}

void EvAlChartPanel::SetChart(std::shared_ptr<EvAlChart> _chart) {
    chart = _chart;
}

#include "stdafx.h"
#include "DBConnDlg.h"

wxBEGIN_EVENT_TABLE(CDBConnDlg, wxDialog)
    EVT_BUTTON(RBUTCONN,OnButConnect)
    EVT_BUTTON(RBUTCLOSE,OnButClose)
wxEND_EVENT_TABLE()


void CDBConnDlg::OnButConnect(wxCommandEvent &event) {

    if(textconnstr->GetLineText(0) == "") {
        wxMessageDialog(this, "Der Verbindungstext darf nicht leer sein.", "Fehler");
        return;
    }

    EndModal(wxOK);
}

CDBConnDlg::CDBConnDlg() {
}


void CDBConnDlg::OnButClose(wxCommandEvent &event) {
    EndModal(wxCANCEL);
}

std::string CDBConnDlg::GetConnStr() {
    return std::string();
}

CDBConnDlg::~CDBConnDlg() {
}

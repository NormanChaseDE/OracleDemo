#pragma once
#include <Windows.h>

#include <vector>
#include <iostream>

#include <sstream>
#include <wx\wx.h>

//#pragma managed(push, off)
#include <occi.h>
#include <oratypes.h>
//#pragma managed(pop)

using namespace oracle;


#pragma comment(lib,"oraocci12d.lib")

class CDb {
private:
    occi::Environment *env = nullptr;
    occi::Connection *conn = nullptr;
    occi::Statement *stmt = nullptr;

    std::string connstr = "Driver={Oracle in instantclient_12_2};DBQ=localhost:1521/oracle;Uid=844143;Pwd=student;";
public:
    CDb();
    ~CDb();
    occi::ResultSet *ExecQuery(std::string query);
    CDb(char *_connstr) :connstr(_connstr) {};

    bool Connect(std::string user, std::string password, std::string connstr, std::vector<wxString> &vmessage);
};


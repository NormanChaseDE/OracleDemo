#include "stdafx.h"
#include "Db.h"

#define OTL_STL

CDb::CDb() {
}


CDb::~CDb() {
    // clean up db
    if(conn) {
        conn->terminateStatement(stmt);
        env->terminateConnection(conn);
    }

    if(env) {
        occi::Environment::terminateEnvironment(env);
    }

}

occi::ResultSet *CDb::ExecQuery(std::string query) {
    return stmt->executeQuery(query);
}

bool CDb::Connect(std::string user, std::string password, std::string connstr, std::vector<wxString> &vmessage) {

    //create environment and connection

    try {

        env = occi::Environment::createEnvironment();
        {
            conn = env->createConnection(user, password, connstr);

            stmt = conn->createStatement();
        }

    } catch(const occi::SQLException &ex) {

        vmessage.push_back("CDb::SQLException:");

        vmessage.push_back(ex.what());
        throw(std::exception(ex.getMessage().c_str()));
        return false;


    }
    return true;

}




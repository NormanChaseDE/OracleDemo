// OracleTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "OracleTest.h"

wxBEGIN_EVENT_TABLE(COracleTest, wxApp)
    EVT_BUTTON(RBUTVIS, OnButVis)
    EVT_COMBOBOX(RCOMBSTMT, OnComboStmt)
    EVT_COMBOBOX(RCOMBSUB, OnComboSub)
wxEND_EVENT_TABLE()

void COracleTest::ShowStatement() {

    if(!db) {  // test if we have a valid db obj
        wxMessageBox(L"Es besteht keine g�ltige Verbindung zur Datenbank.\nDas Programm wird nun beendet.", "Fehler");
        Exit();
        wxApp::CleanUp();
        return;
    }

    try {

        if(stmtid == 0) {
            wxBusyCursor curbusy;

            std::string strstmt = "select name, maxcount, day_date from (select name, max(count) as maxcount from(select teams.name as name, daycount.count as count, DAYCOUNT.DAY_DATE as Day from \"852219\".DAYCOUNT join \"852219\".teams on team_id = teams.ID order by daycount.count desc) group by name order by max(count) desc)  join \"852219\".DAYCOUNT on maxcount = daycount.count order by day_date asc ";

            auto rs = db->ExecQuery(strstmt);

            textstmt->Clear();
            *textstmt << strstmt;

            std::vector<std::string> vdates;
            std::vector<int> vcount;
            std::vector<std::string> vteams;

            while(rs->next()) {
                auto strteam = rs->getString(1);
                auto count = rs->getInt(2);
                auto strdate = rs->getString(3);

                vdates.push_back(strdate);
                vcount.push_back(count);
                vteams.push_back(strteam);
            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());


            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData("An welchem Tag wurde nach welchem Team am h�ufigsten gesucht?", "Datum", "H�ufigkeit", vdates, vcount, vteams));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();
        } else if(stmtid == 1) {
            wxBusyCursor curbusy;
            std::string strstmt = "select date1, count(date1) from (select CAST(cast(querytime as date) as varchar(10)) as date1, team_id  from \"852219\".nbaquery where team_id = 1 order by querytime) where date1> DATE '2006-04-21' group by date1 order by count(date1) desc";

            textstmt->Clear();
            *textstmt << strstmt;

            auto rs = db->ExecQuery(strstmt);

            std::vector<std::string> vdates;
            std::vector<int> vcount;
            while(rs->next()) {
                auto strdate = rs->getString(1);
                auto count = rs->getInt(2);
                //auto strdate = rs->getString(3);

                vdates.push_back(strdate);
                vcount.push_back(count);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData("Welcher Zusammenhang besteht zwischen der H�ufigkeit der Suchanfragen an einem bestimmten Tag\nund dem Stattfinden eines Playoff-Spiels?", "Datum", "Aufrufe", vdates, vcount));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();
        } else if(stmtid == 2) {
            wxBusyCursor curbusy;

            // extract team name from combo string
            wxString strteam = combosub->GetValue();

            int pos = strteam.Find(' ',true);
            strteam = strteam.Right(strteam.Length() - pos - 1);


            std::stringstream sstrstmt;
            sstrstmt<<"select * from (select site_rank, cast(clickurl as varchar(50)), clickcnt from(select rank() over(order by clickcnt DESC) as site_rank, clickurl, clickcnt from(select count(clickurl) as clickcnt, clickurl from \"852219\".nbaquery where team_id in(SELECT id from \"852219\".teams WHERE name = '" << strteam << "') group by clickurl order by count(clickurl) desc))where site_rank <= 3)";

            textstmt->Clear();
            *textstmt << sstrstmt.str();

            auto rs = db->ExecQuery(sstrstmt.str());

            std::vector<std::string> vrank;
            std::vector<std::string> vurl;
            std::vector<int> vcount;

            while(rs->next()) {
                auto rank = rs->getString(1);
                auto strurl = rs->getString(2);
                auto count = rs->getInt(3);


                vrank.push_back(rank);
                vurl.push_back(strurl);
                vcount.push_back(count);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData(wxString::Format("Welche 3 Webseiten wurden am h�ufigsten in Zusammenhang mit %s%s geklickt?", (combosub->GetValue().Find("Heat") > 0) ? "" : "den ", combosub->GetValue()).ToStdString(), "Site_Rank", "Aufrufe", vrank, vcount, vurl));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();

        } else if(stmtid == 3) {
            wxBusyCursor curbusy;
            std::string strstmt = "select \"852219\".Teams.Name, Query_Count from (select count(team_id) as Query_Count, team_id from \"852219\".nbaquery group by team_id order by count(team_id) desc) join \"852219\".Teams on team_id = \"852219\".Teams.Id order by query_count desc";

            textstmt->Clear();
            *textstmt << strstmt;

            auto rs = db->ExecQuery(strstmt);

            std::vector<std::string> vteam;
            std::vector<int> vcount;
            while(rs->next()) {
                auto strteam = rs->getString(1);
                auto count = rs->getInt(2);

                vteam.push_back(strteam);
                vcount.push_back(count);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData("Welches Team besitzt die meisten Suchanfragen?", "Team", "Aufrufe", vteam, vcount));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();

        } else if(stmtid == 4) {
            wxBusyCursor curbusy;
            std::string strstmt = "select count(query) as Count, EXTRACT(hour from QUERYTIME) as Hour from \"852219\".NBAQUERY group by EXTRACT(hour from QUERYTIME) order by EXTRACT(hour from QUERYTIME) asc";

            textstmt->Clear();
            *textstmt << strstmt;

            auto rs = db->ExecQuery(strstmt);

            std::vector<int> vcount;
            std::vector<std::string> vtime;

            while(rs->next()) {
                auto count = rs->getInt(1);
                auto strtime = rs->getString(2)+" Uhr";

                vtime.push_back(strtime);
                vcount.push_back(count);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData("Zu welchen Zeiten suchten User nach ihren Mannschaften?", "Team", "Aufrufe", vtime, vcount));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();

        } else if(stmtid == 5) {
            wxBusyCursor curbusy;
            std::string strstmt = "select count(query) as Count, EXTRACT(hour from QUERYTIME) as Hour from aoldata.querydata where query like '%nba%' and (query like '%score%' or query like '%result%') group by EXTRACT(hour from QUERYTIME) order by EXTRACT(hour from QUERYTIME) asc";

            textstmt->Clear();
            *textstmt << strstmt;

            auto rs = db->ExecQuery(strstmt);

            std::vector<int> vcount;
            std::vector<std::string> vhour;

            while(rs->next()) {
                auto count = rs->getInt(1);
                auto strhour = rs->getString(2)+" Uhr";

                vcount.push_back(count);
                vhour.push_back(strhour);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData("Zu welchen Zeiten wurde nach Spielergebnissen (�score� oder �result�) gesucht?", "Zeit", "Anfragen", vhour, vcount));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();

        } else if(stmtid == 6) {
            wxBusyCursor curbusy;
            std::string strstmt = "select name, cnt from(select * from(select team_id, count(team_id) as cnt from \"852219\".nbaquery group by team_id order by count(team_id) desc) join \"852219\".teams on team_id = id)";

            textstmt->Clear();
            *textstmt << strstmt;

            auto rs = db->ExecQuery(strstmt);

            std::vector<int> vcount;
            std::vector<std::string> vname;

            while(rs->next()) {
                auto strname = rs->getString(1);
                auto count = rs->getInt(2);

                vcount.push_back(count);
                vname.push_back(strname);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData("Gibt es einen Zusammenhang zwischen der absoluten Anfrageh�ufigkeit und dem Tabellenstand der Liga?", "Team", "H�ufigkeit", vname, vcount));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();

        } else if(stmtid == 7) {
            wxBusyCursor curbusy;
            std::string strstmt = "select * from (select count(query) as cnt, CAST(QUERYTIME as date) as qdate from AOLDATA.Querydata where query like '%nba%' and query like '%playoff%' group by CAST(QUERYTIME as date) order by count(query) desc) where cnt>2";
            textstmt->Clear();
            *textstmt << strstmt;

            auto rs = db->ExecQuery(strstmt);

            std::vector<int> vcount;
            std::vector<std::string> vdate;

            while(rs->next()) {
                auto count = rs->getInt(1);
                auto strdate = rs->getString(2);

                vcount.push_back(count);
                vdate.push_back(strdate);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData("Wie oft und wann suchten User nach �NBA� und �Playoffs�?", "Team", "H�ufigkeit", vdate, vcount));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();

        } else if(stmtid == 8) {
            wxBusyCursor curbusy;
            std::string strstmt = "select count(query), trunc(CAST(querytime as date)) from(select * from AOLDATA.QUERYDATA where(query LIKE '%nba%' or query LIKE '%basketball%') AND query LIKE '%tickets%') group by trunc(CAST(querytime as date)) order by  trunc(CAST(querytime as date))";
            textstmt->Clear();
            *textstmt << strstmt;

            auto rs = db->ExecQuery(strstmt);


            std::vector<int> vcount;
            std::vector<std::string> vdate;

            while(rs->next()) {
                auto count = rs->getInt(1);
                auto strdate = rs->getString(2);

                vcount.push_back(count);
                vdate.push_back(strdate);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData("An welchen Tagen und wie oft wurde nach NBA- oder Basketballtickets gesucht?", "Datum", "H�ufigkeit", vdate, vcount));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();

        } else if(stmtid == 9) { //werbefirmen
            wxBusyCursor curbusy;

            wxString strsponsor = combosub->GetValue();

            // extract sponsor from combo

            int found = strsponsor.Find('|');
            strsponsor = strsponsor.Right(strsponsor.Length() - found - 2);

            // extract team from combo

            wxString strteam = combosub->GetValue();
            found = strteam.Find('|');
            strteam = strteam.Left(found - 1);

            std::stringstream sstrstmt;

            sstrstmt<<"select count(qdate),qdate from (select trunc(querytime) as qdate, count(*) as cnt from(select * from aoldata.querydata where query like '%"<<strsponsor.Lower()<<"%') group by querytime order by CAST(querytime as DATE) asc) group by qdate order by qdate";
            textstmt->Clear();
            *textstmt << sstrstmt.str();

            auto rs = db->ExecQuery(sstrstmt.str());

            std::vector<int> vcount;
            std::vector<std::string> vdate;

            while(rs->next()) {
                auto count = rs->getInt(1);
                auto strdate = rs->getString(2);

                vcount.push_back(count);
                vdate.push_back(strdate);

            }

            std::shared_ptr<EvAlBarChart> barchart = std::make_shared<EvAlBarChart>(EvAlBarChart());
            std::shared_ptr<EvAlBarChartData> bcdata = std::make_shared<EvAlBarChartData>(EvAlBarChartData(wxString::Format("An welchen Tagen und wie oft wurde der Stadionsponsor (%s) der %s gesucht?",strsponsor, strteam).ToStdString(), "Team", "H�ufigkeit", vdate, vcount));
            barchart->SetData(bcdata);
            chartpanel->SetChart(barchart);
            chartpanel->Refresh();

        } else {

            wxMessageBox(L"Ung�ltige Statement-Auswahl","Fehler");
        }

    } catch(const occi::SQLException &ex) {
        wxString strex = wxString::Format("Code: %i\n%s", ex.getErrorCode(), ex.what());

        if(!ex.isRecoverable())
            strex.Append("\n\nDas Programm wird nun beendet.");

        wxMessageBox(strex, "SQL-Ausnahme aufgetreten");

        if(!ex.isRecoverable()) {
            Exit();
            wxApp::CleanUp();
        }
    } catch(const std::exception &ex) {
        wxString strex = wxString::Format("%s\n\nDas Programm wird nun beendet.",ex.what());
        wxMessageBox(strex, "Allgemeine Ausnahme aufgetreten");

        Exit();

    }
}

void COracleTest::OnComboStmt(wxCommandEvent &WXUNUSED(event)) {
    stmtid = combostmt->GetSelection();
    try {
        if(stmtid == 2) {
            combosub->Clear();
            std::string strteams = "SELECT \"852219\".city.name, \"852219\".teams.name FROM \"852219\".teams JOIN \"852219\".city ON \"852219\".teams.city_id = \"852219\".city.id";

            // get teams
            auto rs = db->ExecQuery(strteams);

            while(rs->next()) {

                combosub->AppendString(rs->getString(1)+" "+ rs->getString(2));
            }

            combosub->Select(0);
            combosub->Enable(true);
        } else if(stmtid == 9) {
            combosub->Clear();
            std::string strteams = "SELECT * FROM \"852219\".teams";

            // get teams
            auto rs = db->ExecQuery(strteams);

            std::vector<std::string> vteamname;
            std::vector<int> vteamid;

            while(rs->next()) {
                if(rs->getString(5)!="") // skip teams with unknown sponsors
                    combosub->AppendString(wxString::Format("%s | %s", rs->getString(1), rs->getString(5)));
            }

            combosub->Select(0);
            combosub->Enable(true);
        } else {
            combosub->Clear();
            combosub->Enable(false);
        }

    } catch(const std::exception &ex) {
        wxString strex = wxString::Format("%s\n%s\n\nDas Programm wird nun beendet.", ex.what());
        wxMessageBox(strex, "Allgemeine Ausnahme aufgetreten");

        Exit();
        wxApp::CleanUp();
    }

    ShowStatement();
}

void COracleTest::OnComboSub(wxCommandEvent &WXUNUSED) {
    subid = combosub->GetSelection();

    ShowStatement();

}

void COracleTest::OnButVis(wxCommandEvent &WXUNUSED(event)) {
    stmtid = combostmt->GetSelection();

    ShowStatement();
};

unsigned int COracleTest::Connect(std::string strconnect) {
    try {
        db->Connect("844143", "student", "localhost:1521/oracle", vdbmsg);

        return 1;
    } catch(const occi::SQLException &ex) {
        throw(ex);
        return 0;
    }
};

bool COracleTest::OnInit() {

    try {


        mainframe = new wxFrame(NULL, RMAINFRAME, "Oracle Database Test", wxDefaultPosition, { 1024, 768});

        if(!mainframe)
            throw(std::exception("Fehler beim Erstellen des Mainframes."));


        // setup db connection
        db = new CDb();

        if(!db)
            throw(std::exception("Fehler beim Erstellen des Datenbankobjekts."));

        std::string strconnect = "localhost:1521/oracle";

        while(Connect(strconnect) != 1) {
            CDBConnDlg dlg(mainframe, wxID_ANY, strconnect);

            int ret = dlg.ShowModal();

            if(ret == wxOK) {
                strconnect = dlg.GetConnStr();
            } else {
                Exit();
            }
        };

        // create main panel

        mainpanel = new wxPanel(mainframe, RMAINPANEL);

        if(!mainpanel)
            throw(std::exception("Fehler beim Erstellen des Mainpanels."));

        // create chart panel

        chartpanel = new EvAlChartPanel(mainpanel, RCHARTPANEL);
        //chartpanel = new EvAlChartPanel(mainpanel, RCHARTPANEL, { 10,10 }, { 990,600 });

        if(!chartpanel)
            throw(std::exception("Fehler beim Erstellen des Chartpanels."));



        chartpanel->SetBackgroundColour(*wxWHITE);

        // statement combo
        wxArrayString lbentries;

        for(int i = 1; i <= 10; i++) {
            wxString str = wxString::Format("Anfrage %i", i);
            lbentries.Add(str);
        }

        combostmt = new wxComboBox(mainpanel, RCOMBSTMT, "Anfrage 1",wxDefaultPosition, wxDefaultSize, lbentries, wxCB_READONLY|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL);

        if(!combostmt)
            throw(std::exception("Fehler beim Erstellen der Statement-Combobox."));


        combostmt->Set(lbentries);
        combostmt->Select(0);
        combostmt->SetExtraStyle(wxCB_READONLY);

        // combo sub
        combosub = new wxComboBox(mainpanel, RCOMBSUB, "", wxDefaultPosition, wxDefaultSize, wxArrayString(), wxCB_READONLY);

        if(!combosub)
            throw(std::exception("Fehler beim Erstellen der Select-Combobox."));

        combosub->SetExtraStyle(wxCB_READONLY);

        combosub->Enable(false);

        // statement text ctrl
        textstmt = new wxTextCtrl(mainpanel, RSTMTTEXT, "");
        //textstmt = new wxTextCtrl(mainpanel, RSTMTTEXT, "", { 220,620 }, { 780,100 }, wxTE_READONLY | wxTE_MULTILINE);

        if(!textstmt)
            throw(std::exception("Fehler beim Erstellen der Statement-Textcontrol."));

        textstmt->SetExtraStyle(wxTE_READONLY | wxTE_MULTILINE);

        // print db msgs to edit ctrl
        for(auto str : vdbmsg)
            textstmt->AppendText(wxString::Format("%s\r\n", str));


        // create vis button
        butvisualize = new wxButton(mainpanel, RBUTVIS, "Visualisieren!", { 10,620 });

        if(!butvisualize)
            throw(std::exception("Fehler beim Erstellen der Visualisieren-Buttons."));

        // create and set sizers
        szrmain = new wxBoxSizer(wxVERTICAL);
        szrchart = new wxBoxSizer(wxHORIZONTAL);
        szrtext = new wxBoxSizer(wxHORIZONTAL);
        szrcontrols = new wxBoxSizer(wxHORIZONTAL);


        szrchart->Add(chartpanel, 1, wxEXPAND | wxALL, 5);

        szrtext->Add(textstmt, 1, wxEXPAND | wxALL, 5);

        szrcontrols->Add(butvisualize, 1, wxEXPAND | wxALL, 5);
        szrcontrols->Add(combostmt, 1, wxEXPAND | wxALL, 5);
        szrcontrols->Add(combosub, 1, wxEXPAND | wxALL, 5);

        szrmain->Add(szrchart, 10, wxEXPAND | wxALL, 5);
        szrmain->Add(szrtext, 3, wxEXPAND | wxALL, 5);
        szrmain->Add(szrcontrols, 1, wxEXPAND | wxALL, 5);

        mainpanel->SetSizer(szrmain);

        mainframe->Show();

        OnButVis(wxCommandEvent());

    } catch(const std::exception &ex) {
        wxString strex = wxString::Format("Beim Initialisieren des Fensters ist eine schwere Ausnahme aufgetreten.\nWeitere Informationen:\n%s\nDas Programm wird nun beendet.", ex.what());
        wxMessageBox(strex, "Ausnahme");

        OnExit();
        wxApp::CleanUp();
    }

    return true;
}
int COracleTest::OnExit() {

    wxApp::CleanUp();

    if(db)
        delete db;

    return 0;
}



#pragma once
//
//#include <Windows.h>
//#include <sql.h>
//#include <sqlext.h>
#include <string>

#include <iostream>


#include <msvc\wx\setup.h>
#include <wx\wx.h>
#include <wx\combobox.h>
#include <wx\listbox.h>
#include <wx\textctrl.h>

#include "Db.h"
#include "EvAlChartPanel.h"
#include "EvalBarChart.h"
#include "DBConnDlg.h"

#include <memory>

#pragma comment(lib,"wxmsw31ud_core.lib")
#pragma comment(lib,"wxbase31ud.lib")


// defines

#define wxUSE_LISTBOX 1
#define wxUSE_COMBOBOX 1

#define RMAINFRAME 1
#define RMAINPANEL 2
#define RCHARTPANEL 3
#define RSTMTTEXT 4
#define RBUTVIS 5
#define RCOMBSTMT 6
#define RCOMBSUB 7


class COracleTest : public wxApp {
private:
    std::vector<wxString> vdbmsg;

    // controls and panels
    wxComboBox *combostmt = nullptr;
    wxComboBox *combosub = nullptr;
    wxFrame *mainframe=nullptr;
    wxTextCtrl *textstmt = nullptr;
    EvAlChartPanel *chartpanel = nullptr;
    wxPanel *mainpanel = nullptr;
    wxButton *butvisualize = nullptr;

    // sizers
    wxBoxSizer *szrmain = nullptr;
    wxBoxSizer *szrchart = nullptr;
    wxBoxSizer *szrtext = nullptr;
    wxBoxSizer *szrcontrols = nullptr;

    int stmtid = 0;
    int subid = 0;
    CDb *db;
public:
    unsigned int Connect(std::string strconnect);

    void ShowStatement();

    void OnComboStmt(wxCommandEvent &WXUNUSED);

    void OnComboSub(wxCommandEvent &WXUNUSED);

    void OnButVis(wxCommandEvent &WXUNUSED(event));

    virtual bool OnInit();

    virtual int OnExit();

    wxDECLARE_EVENT_TABLE();
};

IMPLEMENT_APP(COracleTest)
#include "stdafx.h"
#include "EvalBarChart.h"



bool EvAlBarChart::IsChartScrollable() {
    if(!data)
        return false;

    if(data->GetVData().size() > barsperpage)
        return true;
    else
        return false;
}

// draws a barchart on a wxPaintDc
//void EvAlBarChart::Draw(wxAutoBufferedPaintDC  *pdc) {
void EvAlBarChart::Draw(wxBufferedPaintDC   *pdc, wxSize szpaintarea) {
    if(pdc) {
        pdc->Clear();

        //auto szpanel = pdc->GetSize();
        auto szpanel = szpaintarea; // fix for broken wxBufferedPaintDC

        auto vaxisdata = data->GetVAxisData();
        auto vdata = data->GetVData();
        auto valmax = data->GetValMax();

        // calc rects
        double bordfactlt = 0.15;
        double bordfactbtm = 0.7;
        double bordfacttop = 0.1;

        wxRect rctitle(0, 0, szpanel.GetX(), (int)(szpanel.GetY()*bordfacttop));
        wxRect rcinner((int)(szpanel.GetX()*bordfactlt), (int)(szpanel.GetY()*bordfacttop), szpanel.GetX()*(1 - bordfactlt), (int)(szpanel.GetY()*bordfactbtm));

        // construct fonts
        wxFont fnttitle(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
        wxFont fntaxislbl(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
        wxFont fntaxissublbl(8, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_ITALIC, wxFONTWEIGHT_NORMAL);
        wxFont fntnorm(8, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);

        //draw title
        auto strtitle = data->GetTitle();
        auto extitle = pdc->GetTextExtent(strtitle);

        pdc->SetFont(fnttitle);
        pdc->SetTextForeground(*wxBLACK);
        pdc->DrawLabel(strtitle, rctitle, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL);  // draw axis labels

        //draw x axis
        pdc->DrawLine({rcinner.GetLeft(),rcinner.GetBottom()}, {rcinner.GetRight(),rcinner.GetBottom()});

        // draw y axis
        pdc->DrawLine({rcinner.GetLeft(),rcinner.GetTop() }, {rcinner.GetLeft(),rcinner.GetBottom() });

        double nexty = rcinner.GetY();
        double yspacing = (double)(rcinner.GetHeight() / 50.0f); // draw only 50 y axis lines


        // draw y axis value lbls
        pdc->SetFont(fntnorm);

        for(double nexty = rcinner.GetY(), i = 0; nexty <= rcinner.GetBottom(); nexty += yspacing, i++) {

            auto strxsublbl = wxString::Format("%.2f", (double)(valmax - i*(valmax / 50.0f)));
            auto exxsublbl = pdc->GetTextExtent(strxsublbl);

            if((int)i % 5 == 0 || i == 0) { // draw only the first and every fifth lable
                pdc->DrawLabel(strxsublbl, { (int)(rcinner.GetLeft() *0.4), (int)nexty, (int)(rcinner.GetLeft()*0.35),exxsublbl.GetHeight() },wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT);
                pdc->DrawLine({ (int)(rcinner.GetLeft() *0.75),(int)nexty }, { rcinner.GetLeft(),(int)nexty });
            } else
                pdc->DrawLine({ (int)(rcinner.GetLeft() *0.8),(int)nexty }, { rcinner.GetLeft(),(int)nexty });
        }

        // draw x lable
        pdc->SetFont(fntaxislbl);

        auto exxlbl = pdc->GetTextExtent(data->GetXAxisLabel());
        pdc->DrawRotatedText(data->GetXAxisLabel(), { (int)(rcinner.GetLeft() *0.125-exxlbl.GetHeight()*0.5),(int)(rcinner.GetY()+rcinner.GetHeight()*0.5 -exxlbl.GetWidth()*0.5)}, 90.0);


        auto rcxlabel = wxRect(rcinner.GetLeft(), rcinner.GetBottom() + (szpanel.GetY() - rcinner.GetBottom())*0.75, rcinner.GetWidth(), (szpanel.GetY() - rcinner.GetBottom())*0.25);
        auto exylbl = pdc->GetTextExtent(data->GetXAxisLabel());
        pdc->DrawLabel(data->GetYAxisLabel(), rcxlabel,wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL);


        // draw bars
        pdc->SetFont(fntnorm);

        auto itval = vdata.begin() += valstart; // get iterator to current start of data
        auto itvaxisdata = vaxisdata.begin() += valstart;// get iterator to current start of axis data

        // acquire iterator to hold starting point of vbardata vector
        std::vector<std::string>::iterator itvbardata;

        if(data->GetVBarLabels().size()>0)
            itvbardata = data->GetVBarLabels().begin() += valstart;// get iterator to current start of axis sublable data

        double barbox;

        if(vdata.size()>barsperpage) // more data in vector than can fit on screen
            barbox = (rcinner.GetWidth() / barsperpage);
        else
            barbox = (rcinner.GetWidth() / vdata.size()); // calc possible bar size

        int nextx = rcinner.GetLeft(); // next bar starting point

        unsigned int valcounter = 0; // keeps track of drawn bars



        while(itval!=std::end(vdata)) {

            double barh = (((double)*itval / valmax))*(double)rcinner.GetHeight();

            wxRect rcbar(wxPoint(nextx + barbox*0.05, rcinner.GetHeight() - barh + rcinner.GetY()), wxPoint(nextx + barbox*0.9, rcinner.GetBottom()));
            wxRect rcaxislbl(nextx, (rcinner.GetBottom()), barbox, (szpanel.GetY() - rcinner.GetBottom()) *0.75);

            // draw shadow
            if(*itval != 0) {   // neither shadow or bar if val is zero
                wxRect rcShadow(wxPoint(nextx + barbox*0.1, rcinner.GetHeight() - barh + rcinner.GetY() + 2), wxPoint(nextx + barbox*0.95, rcinner.GetBottom()));
                pdc->SetPen(*wxBLACK_PEN);
                pdc->SetBrush(wxColor(50, 50, 50));
                pdc->DrawRectangle(rcShadow);


                // draw bar
                pdc->SetPen(*wxTRANSPARENT_PEN);
                pdc->SetBrush(wxColor(71, 113, 165));
                pdc->DrawRectangle(rcbar);
            }

            // draw value on bar
            auto strval = wxString::Format("%i", *itval);
            auto exvalue = pdc->GetTextExtent(strval);

            if(*itval != 0) {


                if(exvalue.GetHeight() > barh) {   // draw text above bar if it doesn't fit
                    pdc->SetTextForeground(*wxBLACK);
                    pdc->DrawText(strval, rcaxislbl.GetRight() - rcaxislbl.GetWidth() / 2 - exvalue.GetWidth() / 2, rcinner.GetBottom() - barh - exvalue.GetHeight());  // draw axis labels
                } else {
                    pdc->SetTextForeground(*wxWHITE);
                    pdc->DrawText(strval, rcaxislbl.GetRight() - rcaxislbl.GetWidth() / 2 - exvalue.GetWidth() / 2, rcinner.GetHeight() - barh + rcinner.GetY());  // draw axis labels
                }
            } else { // draw value above axis for zero values
                pdc->SetTextForeground(*wxBLACK);
                pdc->DrawText(strval, rcaxislbl.GetRight() - rcaxislbl.GetWidth() / 2 - exvalue.GetWidth() / 2, rcinner.GetBottom() - exvalue.GetHeight());  // draw axis labels
            }



            // draw axis lable
            pdc->SetFont(fntnorm);
            auto exlbl = pdc->GetTextExtent(*itvaxisdata);

            //check if axis lable is too big to fit
            pdc->SetTextForeground(*wxBLACK);

            if(exlbl.GetWidth() > rcaxislbl.GetWidth()) {  // yes it is too big
                //pdc->DrawText(*itvaxisdata, nextx + barbox / 2, rcinner.GetBottom()); // draw axis labels
                int axislblspace = 1;


                // test if it fits rotated
                if(exlbl.GetWidth() < rcaxislbl.GetHeight()) {
                    pdc->DrawRotatedText(*itvaxisdata, wxPoint(rcaxislbl.GetLeft() + rcaxislbl.GetWidth()*0.5- exlbl.GetHeight() *0.5, rcaxislbl.GetTop()+exlbl.GetWidth()+ axislblspace), 90.0);
                } else {// it doesn't fit

                    //fit our lable into the axis lbl rect by adding ...
                    auto strxaxis = wxString(*itvaxisdata);

                    wxString strclip;
                    wxSize exstrxaxis;
                    for(int i = strxaxis.Length(); i > 0; i--) {

                        strclip = strxaxis.Left(i).Append("...");

                        exstrxaxis = pdc->GetTextExtent(strclip);

                        if(exstrxaxis.GetWidth() < rcaxislbl.GetHeight())  // yes, we found our string
                            break;
                    }

                    pdc->DrawRotatedText(strclip, wxPoint(rcaxislbl.GetLeft() + exlbl.GetHeight() / 2.0, rcaxislbl.GetTop() + exstrxaxis.GetWidth()+1 /* 3 pixel space */), 90.0);
                }

            } else // center text in axis lbl rect
                pdc->DrawLabel(*itvaxisdata, rcaxislbl, wxALIGN_CENTER_HORIZONTAL | wxALIGN_TOP);

            // draw bar sublabels
            if(data->GetVBarLabels().size() > 0) {
                int sublblspace = 5;

                pdc->SetFont(fntaxissublbl);

                wxString strbarlbl = *itvbardata;
                auto exbarlbl = pdc->GetTextExtent(strbarlbl);

                if(exbarlbl.GetWidth() > rcbar.GetHeight() - exvalue.GetHeight() - sublblspace) {   // draw above bar, if sublabel is too big for bar
                    pdc->SetTextForeground(wxColor(0, 0, 0));
                    pdc->DrawRotatedText(strbarlbl, wxPoint(rcbar.GetLeft() + rcbar.GetWidth()*0.5 - exbarlbl.GetHeight() *0.5, rcbar.GetTop() - sublblspace), 90.0);
                } else {
                    pdc->SetTextForeground(wxColor(255, 255, 255));
                    pdc->DrawRotatedText(strbarlbl, wxPoint(rcbar.GetLeft() + rcbar.GetWidth()*0.5 - exbarlbl.GetHeight() *0.5, rcinner.GetBottom() - sublblspace), 90.0);
                }

                itvbardata++;
                valcounter++;

                if(valcounter > barsperpage+1) // have we drawn all posible bars?
                    break;

            }


            nextx += barbox; // step up to next bar
            itvaxisdata++;
            itval++;
        }

    }
}

void EvAlBarChart::Scroll(int dir) {

    if(!data->GetVData().size())  // no data set
        return;

    if(data->GetVData().size() <= barsperpage)  // no scrolling if amount of bars is less than limit (barsperpage)
        return;

    if(dir < 0) {
        valstart+= scrollnumbars;

        if(valstart > data->GetVData().size() - barsperpage -2) {
            valstart = data->GetVData().size() - barsperpage - 2;
            //OutputDebugString("valstart > data->GetVData().size() - barsperpage\n\0");
        }

        wxString strdeb = wxString::Format(L"Valstart inc = %i\n", valstart);
        //OutputDebugString(LPCWSTR(strdeb));
    } else {
        valstart-= scrollnumbars;

        if(valstart < 0) {
            valstart = 0;

            //OutputDebugString(LPCWSTR("valstart < 0\n\0"));
        }

        wxString strdeb = wxString::Format(L"Valstart dec = %i\n", valstart);
        //OutputDebugString(LPCWSTR(strdeb));
    }




}

void EvAlBarChart::SetData(std::shared_ptr<EvAlBarChartData> _data) {
    data =_data;
}

void EvAlBarChart::SetPage(int _page) {
    valstart = _page;
}

EvAlBarChart::EvAlBarChart() {
}

EvAlBarChart::~EvAlBarChart() {

}


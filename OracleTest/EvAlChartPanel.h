#pragma once

#include <wx\wx.h>
#include <wx\event.h>
#include <wx/dcbuffer.h>

#include "EvAlChart.h"
#include "EvalBarChart.h"

#include <memory>

class EvAlChart;

class EvAlChartPanel:public wxPanel {
private:
    std::shared_ptr<EvAlChart> chart = nullptr;
public:
    void OnMousewheel(wxMouseEvent &event);
    void OnMouseEnter(wxMouseEvent &event);
    virtual void OnSize(wxSizeEvent &event);
    virtual void OnPaint(wxPaintEvent &event);
    EvAlChartPanel(wxWindow   	*parent,
                   wxWindowID  	id = wxID_ANY,
                   const wxPoint   	&pos = wxDefaultPosition,
                   const wxSize   	&size = wxDefaultSize
                  ) :wxPanel(parent, id, pos, size) {
        wxWindow::SetBackgroundStyle(wxBG_STYLE_PAINT); // need for buffered dc
    };

    EvAlChartPanel();
    ~EvAlChartPanel();

    void SetChart(std::shared_ptr<EvAlChart> _chart);
    DECLARE_EVENT_TABLE()
};


#pragma once
#include <wx\dcbuffer.h>

class EvAlChartPanel;

class EvAlChart {
public:
    virtual bool IsChartScrollable() = 0;
    virtual void Draw(wxBufferedPaintDC   *dc, wxSize szpaintarea) = 0;
    virtual void Scroll(int dir) = 0;
};

